#
# run with:
# ruby -rubygems app.rb
# by http://ushcompu.com.ar
#
require 'sinatra'
require 'slim'
require 'mongo_mapper'
require 'require_relative'

# TODO: build style.css with compass / sass
# TODO: build behavior.js with coffeescript

# get '/stylesheet.css' do

MongoMapper.connection = Mongo::Connection.new('0.0.0.0')
MongoMapper.database = 'sinatrodo'
MongoMapper.database.authenticate('sinotrodo','sinotrodo')

APP_NAME = 'sinotrodo'

require_relative "controller/task"
