class Sinotrodo
  require_relative "../model/task"

  get '/' do
    @title = "inicio"
    @tasks = Task.all
    slim :index
  end
  
  post '/new' do
     task = Task.new
     task.name = (params[:name])
  
     if task.save
        status 201
     else
        status 401
     end
  
     redirect '/'
  end

  delete '/:id/delete' do
    Task.find(params[:id]).destroy
    redirect '/'
  end
end
