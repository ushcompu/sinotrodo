.. -*- coding: utf-8 -*-

Sinotrodo
=========
  Sinatra TODO application

Install
-------
  ruby, sinatra, compass, sass, coffeescript, slim, mongomapper_ext

Build assets
------------
  $ cd assets-src/css && ./make.rb && cd -
  $ cd assets-src/js && ./make.rb && cd -
  $ ruby -rubygems.rb /usr/lib/ruby/gems/1.9.1/gems/jammit-0.6.0/bin/jammit #optional to compress & generate .gzhhh:wq

Run
---
  $ ruby -rubygems app.rb
