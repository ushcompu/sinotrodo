class Task
  include MongoMapper::Document

  belongs_to :author

  key :name, String,  :default => ""
  key :prio, Integer, :default => 0

  timestamps!
end
