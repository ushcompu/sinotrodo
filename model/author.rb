class Author
  include MongoMapper::Document

  key :email, String,  :default => ""
  key :name, String,   :default => "X"
  key :pass, String,   :default => "X"

  validates_length_of :email, :minimum => 1, :maximum => 140
  validates_length_of :name,  :minimum => 1, :maximum => 255
  validates_length_of :pass,  :minimum => 6, :maximum => 255
end
