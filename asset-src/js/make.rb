#!/usr/bin/ruby
require 'coffee-script'

Dir.foreach('.') do |cf|
  unless cf == '.' || cf == '..' || cf.end_with?('.coffee') == false
    system("coffee -o ../../public/js -c #{cf}")
  end
end
